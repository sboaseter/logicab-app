angular.module('LogiCAB.controllers', [])

.run(function($rootScope, User, Order, $ionicLoading, Cart, apiUrls, TourGuide) {
  $rootScope.tmpStorage = {};
  $rootScope.formatDate = function(cdate) {
    return moment(cdate).format("DD-MM-YYYY");
  };  
  $rootScope.formatDateDay = function(cdate) {
    return moment(cdate).format("dddd Do MMMM");
  };    
  $rootScope.formatDateLong = function(cdate) {
    return moment(cdate).format("DD-MM-YYYY HH:mm");
  };      
  $rootScope.dateInPast = function(cdate) {
    var date = moment(cdate)
    var now = moment();

    console.log(date);

    if (now > date) {
       // date is past
       return 'Valid';
    } else {
       // date is future
       return 'True';
    }    
  };
  $rootScope.maingroupImage = function(id) {
    if(id == 2) {
      return "img/flower.png";
    } else if(id == 1) {
      return "img/plant.png";
    } else if(id == 4) {
      return "img/misc.png";
    }
  };  
  $rootScope.maingroupText = function(id) {
    if(id == 2) {
      return "Bloemen";
    } else if(id == 1) {
      return "Planten";
    } else if(id == 4) {
      return "Overig";
    }
  };   
  $rootScope.rgetSelectedDate = function() {
    return User.getSelectedDate();
  };
  $rootScope.getSelectedMaingroup = function() {
    return User.getSelectedMaingroup();
  };
  $rootScope.cartCount = function() {
    if(Cart.getCart() != null) {
      return Cart.getCart().length;
    } else {
      return 0;
    }
  };
  $rootScope.getStatusText = function(status) {
  	var statusList = [
  		"NA",
  		"REQ",
  		"CONFIRMED",
  		"REJECTED",
  		"KWBSENT",
  		"EKTKWBSENT",
  		"NEW",
  		"CANCEL",
  		"CHANGEDORDER",
  		"CHANGEDBYGROWER",
  		"ORDERED",
  		"CHANGEDBYBUYER",
  		"SAVEDBYBUYER",
  		"SAVEDBYGROWER",
  		"EKTSENT", 
  		"BUYERRECALL"
  	];
  	return statusList[status];
  };
  $rootScope.ektStatusColor = function(status) {
  	if(status == 14 || status == 5 ) return "green";
  	return "red";
  };
  $rootScope.kwbStatusColor = function(status) {
  	if(status == 4 || status == 5 ) return "green";
  	return "red";
  };  

  $rootScope.imgUrl = function(id) {
    var env = User.getEnviroment();
    var imgurl = env.imgurl + "/ShowImage/" + id;
    return imgurl;
  };

  $rootScope.imgUrlMedium = function(id) {
    var env = User.getEnviroment();
    var imgurl = env.imgurl + "/ShowImageMedium/" + id;
    return imgurl;
  };  

  $rootScope.imgUrlSmall = function(id) {
    var env = User.getEnviroment();
    var imgurl = env.imgurl + "/ShowImageSmall/" + id;
    return imgurl;
  };

  $rootScope.getStatusTextColor = function(status, t) {
    var statusList = [
      "NA", //0
      "REQ",  // 1
      "CONFIRMED", // 2
      "REJECTED", // 3
      "KWBSENT", // 4
      "EKTKWBSENT", // 5
      "NEW", // 6
      "CANCEL", // 7
      "CHANGEDORDER", // 8
      "CHANGEDBYGROWER", // 9
      "ORDERED", // 10
      "CHANGEDBYBUYER", // 11
      "SAVEDBYBUYER", // 12
      "SAVEDBYGROWER", // 13
      "EKTSENT",  // 14
      "BUYERRECALL" // 15
    ];
    var statusListC = [
      "badge-stable",
      "badge-positive",
      "badge-balanced",
      "badge-assertive",
      "badge-balanced",
      "badge-balanced",
      "badge-positive",
      "badge-assertive",
      "badge-royal",
      "badge-royal",
      "badge-positive",
      "badge-royal",
      "badge-royal",
      "badge-royal",
      "badge-balanced", 
      "badge-royal"
    ];    
    if(t==0) {
      return statusList[status];
    }
    if(t==1) {
      return statusListC[status]; 
    }
  };  

  $rootScope.$on('loading:show', function() {
    $ionicLoading.show({template: 'Loading...'})
  });

  $rootScope.$on('loading:hide', function() {
    $ionicLoading.hide()
  }); 

  $rootScope.getNextTour = function() {
    TourGuide.next();
  };
  $rootScope.getPreviousTour = function() {
    TourGuide.prev();
  };
  $rootScope.setTourSeen = function() {
    TourGuide.setSeen();

  };
})
.controller('AppCtrl', function($scope, $state, User, $ionicModal) {
  $scope.model = {};
  $scope.model.isGrower = User.isGrower();
  $scope.model.isBuyer = User.isBuyer();

  $scope.model.user = User.getUser();

  $scope.doLogout = function() {
    User.logout();
    $state.go('login');
  }; 
  // Only works with Android, still lacking good solution for iOS
  $scope.exitApp = function() {
    ionic.Platform.exitApp();
  };  

  $ionicModal.fromTemplateUrl('templates/helpmodal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.helpmodal = modal;
  });

  $scope.sendHelp = function(frmData) {
    console.log(frmData);
    $scope.infoMessage = 'Opmerkingen verstuurd!';

    $scope.helpmodal.hide();
    User.sendFeedback(frmData.remarks).then(function(res) {
      console.log(res);
    },
      function(res) {
        console.log('Error res: ', res);
    })    
  };
})
.controller('ChartsCtrl', function($scope) {})
.controller('SettingsCtrl', function($scope, $state, $ionicHistory, $cordovaStatusbar, User, Camera, apiUrls, $cordovaFileTransfer) {
    $scope.model = {};
    var settings = User.getSettings();
    $scope.model.settings = settings;

    //var ft = new FileTransfer();
    console.log($cordovaFileTransfer);

    $scope.goHome = function() {
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });      
      $state.go('tab.dash');
    };

    $scope.clearEnv = function() {
      User.setEnviroment(apiUrls[0]);     
    };

    $scope.model.daysback = 14; // Store to user-settings object in localstorage
    var fromDate = new Date(); // Candidate for Settings-page
    fromDate.setDate(fromDate.getDate()-$scope.model.daysback);    
    fromDate = moment(fromDate).format();
    $scope.model.FromDate = fromDate;
    $scope.toggleStatusbar = function() {
      $cordovaStatusbar.isVisible() ? $cordovaStatusbar.hide() : $cordovaStatusbar.show()
    };
    $scope.updateSettings = function() {

      if($scope.model.settings.historyDays > 365) { // Prevent massive API-calls!
        $scope.model.settings.historyDays = 365;
      }
      var fromDate = new Date(); // Candidate for Settings-page

      fromDate.setDate(fromDate.getDate()-$scope.model.settings.historyDays);    
      fromDate = moment(fromDate).format();
      $scope.model.FromDate = fromDate;   

      console.log('Tour: ', $scope.model.settings.tour);
      if($scope.model.settings.tour) { // On
        User.resetTour(); // Sets localstorage version to the constant-version tourGuide.
      } else {
        User.disableTour();
      }

      User.setSettings($scope.model.settings);

    };
    $scope.photourl = 'img/special.png';
    $scope.getPhoto = function() {
      Camera.getPicture().then(function(fileURI) {
        console.log('imageURI: ', fileURI);

      var url = "http://apidev.logicab.nl/api/v1/Echo/uploadfile";
//      var file = fileURI.substr(fileURI.lastIndexOf('/')+1);
      var options = {};
      var img = document.createElement("img");
      img.src = fileURI;
      console.log('img.src: ', img.src);      

      var MAX_WIDTH = 800;
      var MAX_HEIGHT = 600;
      var width = img.width;
      var height = img.height;

      console.log('img props: width: ',  img.width, ', height: ', img.height);
       
      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      var canvas = document.getElementsByTagName('canvas')[0];
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0, width, height);
      console.log('Image drawn on canves!');
      var dataurl = canvas.toDataURL("image/png");
      console.log('dataurl: ', dataurl);      

/*        $cordovaFileTransfer.upload(url, fileURI, options)
          .then(function(result) {
            console.log('Success upload: ', JSON.stringify(result));
            // Success!
          }, function(err) {
            console.log('Error upload: ', JSON.stringify(err));
            // Error
          }, function (progress) {
            // constant progress updates
            console.log('Progress upload: ', progress);
          });
*/

//        $scope.photourl = imageURI;
/*        Camera.sendPicture(imageURI).then(function(res) {
          console.log('Camera.sendPicture res: ', res);
        });*/

      }, function(err) {
        console.err(err);
      });
    };    
});
