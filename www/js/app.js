angular.module('LogiCAB', [
  'ionic', 
  'ionic-datepicker', 
  'ionic-timepicker', 
  'ngCordova', 
  'ngStorage',
  'LogiCAB.controllers',
  'LogiCAB.services'
])
.constant(
  'apiUrls', [ 
    { 
      "name": 'Production', 
      "id": 1, 
       "url": 'http://apidev.logicab.nl/api/v1', 
      //"url": 'http://localhost/CABApi/api/v1/', 
      "imgurl": 'http://www.logioffer.nl/Specials' 
    },
    { 
      "name": 'Developement', 
      "id": 2, 
      "url": 'http://apidev.logicab.nl/api/v1', 
      "imgurl": 'http://dev.logioffer.nl/Specials' 
    },    
    { 
      "name": 'Local', 
      "id": 3, 
      "url": 'http://localhost/CABApi/api/v1', 
      "imgurl": 'http://dev.logioffer.nl/Specials' 
    }
  ]
).constant( // 
  'tourGuide', [
    {
      "state": 'tab.offerte',
      "url":  [
        'templates/tour/tab.offerte1.html',
        'templates/tour/tab.offerte2.html',
        'templates/tour/tab.offerte3.html',
        'templates/tour/tab.offerte4.html',
        'templates/tour/tab.offerte5.html'
      ],
      "show": 1
    },  
    {
      "state": 'tab.order',
      "url":  [
        'templates/tour/tab.order1.html',
        'templates/tour/tab.order2.html',
        'templates/tour/tab.order3.html',
        'templates/tour/tab.order4.html',
        'templates/tour/tab.order5.html',
        'templates/tour/tab.order6.html'
      ],
      "show": 1
    },    
    {
      "state": 'tab.offerte-new',
      "url":  [
        'templates/tour/tab.offerte.new1.html',
        'templates/tour/tab.offerte.new2.html',
        'templates/tour/tab.offerte.new3.html'
      ],
      "show": 1
    },  
    {
      "state": 'tab.buyerorder',
      "url":  [
        'templates/tour/tab.buyerorder1.html',
        'templates/tour/tab.buyerorder2.html',
        'templates/tour/tab.buyerorder3.html'
      ],
      "show": 1
    },         
    {
      "state": 'tab.dash',
      "url": 'templates/tour/tab.dash',
      "show": 1
    },
    
  ]
)
.run(function($ionicPlatform, $ionicLoading, $cordovaDevice, $cordovaStatusbar) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
     if(window.StatusBar) {
       StatusBar.styleDefault();
     }

  });  
  moment.locale('nl');
})
.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider, $compileProvider) {
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
  $httpProvider.interceptors.push(function($rootScope) {
    return {
      request: function(config) {
        $rootScope.$broadcast('loading:show')
        return config
      },
      response: function(response) {
        $rootScope.$broadcast('loading:hide')
        return response
      }
    }
  });
  $stateProvider
    .state('login', {
      url: "/login",
      views: {
        '': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html",
      controller: 'AppCtrl'
    })

    .state('tab.dash', {
      url: '/dash',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tab-dash.html',
          controller: 'DashCtrl'
        }
      }
    })
    //Subviews
      .state('tab.dash-news', {
        url: '/dash/news/:newsId',
        views: {
          'tab-dash': {
            templateUrl: 'templates/tab-dash/news.html',
            controller: 'DashNewsCtrl'
          }
        }
      })

      /*
      LogiOfferte Start
      */

      .state('tab.offerte', {
        url: '/offerte',
        views: {
          'tab-offerte': {
            templateUrl: 'templates/tab-offerte.html',
            controller: 'OfferteCtrl'
          }
        }
      })
      .state('tab.offerte-list', {
        url: '/offerte/list',
        views: {
          'tab-offerte': {
            templateUrl: 'templates/tab-offerte/list.html',
            controller: 'OfferteListCtrl'
          }
        }
      }) 
      .state('tab.offerte-new', {
        url: '/offerte/new',
        views: {
          'tab-offerte': {
            templateUrl: 'templates/tab-offerte/new.html',
            controller: 'OfferteNewCtrl'
          }
        }
      })
      .state('tab.offerte-overview', {
        url: '/offerte/overzicht',
        views: {
          'tab-offerte': {
            templateUrl: 'templates/tab-offerte/overview.html',
            controller: 'OfferteOverviewCtrl'
          }
        }
      })            
      .state('tab.offerte-detail', {
        url: '/offerte/detail/:offerteId',
        views: {
          'tab-offerte': {
            templateUrl: 'templates/tab-offerte/detail.html',
            controller: 'OfferteDetailCtrl'
          }
        }
      })    
      .state('tab.offerte-copy', {
        url: '/offerte/copy/:offerteId',
        views: {
          'tab-offerte': {
            templateUrl: 'templates/tab-offerte/copy.html',
            controller: 'OfferteCopyCtrl'
          }
        }
      })    
      .state('tab.offerte-buyers', {
        url: '/offerte/buyers/:offerteId',
        views: {
          'tab-offerte': {
            templateUrl: 'templates/tab-offerte/buyers.html',
            controller: 'OfferteBuyersCtrl'
          }
        }
      })           
    .state('tab.order', {
      url: '/order',
      views: {
        'tab-order': {
          templateUrl: 'templates/tab-order.html',
          controller: 'OrderCtrl'
        }
      }
    })
    .state('tab.order-buyers', {
      url: '/order/:buyerId',
      views: {
        'tab-order': {
          templateUrl: 'templates/tab-order/buyers.html',
          controller: 'OrderBuyersCtrl'
        }
      }
    })    
    .state('tab.order-detail', {
      url: '/order/detail/:orderId',
      views: {
        'tab-order': {
          templateUrl: 'templates/tab-order/detail.html',
          controller: 'OrderDetailCtrl'
        }
      }
    }) 
    /*
      LogiOfferte Stop
    */    

    /*
      LogiOffer Start
    */

    .state('tab.offer', {
      url: '/offer',
      views: {
        'tab-offer': {
          templateUrl: 'templates/tab-offer.html',
          controller: 'OfferCtrl'
        }
      }
    })
  .state('tab.offer-dates', {
    url: '/offer/:mgId',
    views: {
      'tab-offer': {
        templateUrl: 'templates/tab-offer/dates.html',
        controller: 'OfferDatesCtrl'
      }
    }
  })    
  .state('tab.offer-list', {
    url: '/offer/list/:selectedDate',
    views: {
      'tab-offer': {
        templateUrl: 'templates/tab-offer/list.html',
        controller: 'OfferListCtrl'
      }
    }
  })  
  .state('tab.offer-detail', {
    url: '/offer/detail/:ofdt',
    views: {
      'tab-offer': {
        templateUrl: 'templates/tab-offer/detail.html',
        controller: 'OfferDetailCtrl'
      }
    }
  })
    .state('tab.buyerorder', {
      url: '/buyerorder',
      views: {
        'tab-buyerorder': {
          templateUrl: 'templates/tab-buyerorder.html',
          controller: 'BuyerOrderCtrl'
        }
      }
    })    
    .state('tab.buyerorder-detail', {
      url: '/buyerorder/detail/:orderId',
      views: {
        'tab-buyerorder': {
          templateUrl: 'templates/tab-buyerorder/detail.html',
          controller: 'BuyerOrderDetailCtrl'
        }
      }
    })     
    .state('tab.cart', {
      url: '/cart',
      views: {
        'tab-cart': {
          templateUrl: 'templates/tab-cart.html',
          controller: 'CartCtrl'
        }
      }
    })     

    /*
      LogiOffer Stop
    */


    /*
      General states & views
    */  

    .state('tab.dash-settings', {
      url: '/dash/settings',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tab-settings.html',
          controller: 'SettingsCtrl'
        }
      }
    });
/*    .state('tab.charts', {
      url: '/charts',
      views: {
        'tab-charts': {
          templateUrl: 'templates/tab-charts.html',
          controller: 'ChartsCtrl'
        }
      }
    })*/    
    // StateProvider End

  $urlRouterProvider.otherwise('/login');  
  $ionicConfigProvider.backButton.text('').icon('ion-chevron-left').previousTitleText(false);
  $ionicConfigProvider.platform.android.tabs.position('bottom');  
  $ionicConfigProvider.platform.android.navBar.alignTitle('center');
  $ionicConfigProvider.platform.ios.form.checkbox('square');
})
// Move to own directives folder structure(?)
.directive('standardTimeNoMeridian', function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { etime: '=etime' },
        template: "<span>{{stime}}</span>",
        link: function (scope, elem, attrs) {
            scope.stime = epochParser(scope.etime, 'time');
            function prependZero(param) {
                if (String(param).length < 2) {
                    return "0" + String(param);
                }
                return param;
            }
            function epochParser(val, opType) {
                if (val === null) {
                    return "00:00";
                } else {
                    if (opType === 'time') {
                        var hours = parseInt(val / 3600);
                        var minutes = (val / 60) % 60;

                        return (prependZero(hours) + ":" + prependZero(minutes));
                    }
                }
            }
            scope.$watch('etime', function (newValue, oldValue) {
                scope.stime = epochParser(scope.etime, 'time');
            });
        }
    };
});