angular.module('LogiCAB.controllers')
.controller('OrderBuyersCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, Order, TourGuide, $stateParams, $localstorage) {

  console.log($stateParams.buyerId);
  var id = $stateParams.buyerId;

  $scope.model = {};

  $scope.model.message = "All orders from selected buyer: " +id+  "!";

  var orderlist = $localstorage.getObject('orderlist');

  var ordersFromBuyer = null;
  for(var i=0;i<orderlist.length;i++) {
    if(orderlist[i].OrgaSeq == id) {
      ordersFromBuyer = orderlist[i];
    }
  }
//  $scope.model.orderheader = 
  $scope.model.header = ordersFromBuyer;
  console.log('Current orders: ', ordersFromBuyer);

});