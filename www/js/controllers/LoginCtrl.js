angular.module('LogiCAB.controllers')
.controller('LoginCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal) {
	console.log('LoginCtrl');
  $scope.model = {};
  $scope.user = {};
  $scope.model.user = {};
  $scope.model.errormsg = '';
  $scope.enviroments = apiUrls;
	$scope.username = User.getName();
	if(User.rememberMe()) {
		$state.go('tab.dash');	
	}

  $ionicModal.fromTemplateUrl('templates/infomodal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.infoMessage = "Geïnteresseerd in LogiCAB?";

  $scope.saveInformation = function(newUser) {
    console.log(newUser);
    $scope.infoMessage = 'Informatie-aanvraag verstuurd!';

    $scope.modal.hide();
    User.registerUser(newUser).then(function(res) {
      console.log(res);
    })
    
  };

	$scope.doLogin = function() {
		if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.close();
		}
    if($scope.user == undefined  || $scope.user.username == undefined || $scope.user.password == undefined) {
      $scope.model.errormsg = 'Ongeldige gebruikersnaam/wachtwoord';
      return;
    }
		$ionicLoading.show();
    try {
  		User.validate($scope.user.username, $scope.user.password, $scope.user.rememberme, $scope.model.selectedEnv).then(function(result) {
  			$ionicLoading.hide();
  			if(result) {
				  $state.go('tab.dash');	
  			} else {
          $scope.model.errormsg = 'Er is iets misgegaan';
  				$state.go('login');	
  			}
  		});	
    }catch(e) {
      $scope.model.errormsg = 'Er is iets misgegaan';
      $ionicLoading.hide();
    }
	}
});