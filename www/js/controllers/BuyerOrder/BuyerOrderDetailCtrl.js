angular.module('LogiCAB.controllers')
.controller('BuyerOrderDetailCtrl', function(
  $scope, 
  $state, 
  $ionicLoading, 
  $ionicModal, 
  $stateParams, 
  $ionicListDelegate,
  $ionicPopup,
  apiUrls, // Constant
  User, // User-service
  Order // Order-service
  ) {
  var id = $stateParams.orderId;
  console.log("orderID: ", id);
  $scope.model = {};
  $scope.model.orderheader = Order.getById(id)

  Order.listDetail(id).then(function(orderdetails) {
    $scope.model.orderdetails = orderdetails;
  })

  $scope.doRefresh = function() {
    $state.go('tab.buyerorder');
  }   

  $scope.buttons = {};
  $scope.buttons.sendback = {
    disabled: true,
    click: function(id) {
      var data = {
        Seq: id,
        Details: $scope.model.modifieddetails
      };
      $ionicLoading.show();
      Order.modifyOrder(data).then(function(res) {
        $ionicLoading.hide();
        $state.go('tab.order');
      });
    }
  }
  $scope.buttons.reject = {
    disabled: false,
    click: function(id) {
      Order.rejectOrder(id).then(function(res) {
        $state.go('tab.order');
      });
    }
  }
  $scope.buttons.accept = {
    disabled: false,
    click: function(id) {      
      $scope.acceptOrder(id);
    }
  }

 $scope.getLineValue = function(o) {
    return o.ORDE_NUM_OF_UNITS*o.CACA_NUM_OF_UNITS*$scope.getValue(o);
 }
 $scope.getValue = function(o) {
    var price = 0;
    if(typeof o.ORDE_ITEM_PRICE == "string" ) {
      price = parseFloat(o.ORDE_ITEM_PRICE.replace(',', '.'));
    } else {
      price = parseFloat(o.ORDE_ITEM_PRICE);
    }    
    return price;
 } 

  $scope.acceptOrder = function(id) {
    $scope.buttons.accept.disabled = true;
    $ionicLoading.show();
    Order.acceptOrder(id).then(function(res) {
      $ionicLoading.hide();
      $state.go('tab.order');
    });
  }
  $scope.sendEKT = function(id) {
    $ionicLoading.show();
    Order.sendEKT(id).then(function(res) {
      $ionicLoading.hide();
      $state.go('tab.order');
    });
  }
  $scope.sendKWB = function(id) {
    $ionicLoading.show();
    Order.sendKWB(id).then(function(res) {
      $ionicLoading.hide();
      $state.go('tab.order');
    });
  }

  $scope.model.modifieddetails = [];

$scope.model.modified = false;
 $scope.addModification = function(d) {
  var found = false;
  //$scope.model.modified = true;
  $scope.buttons.sendback.disabled = false; // Doesnt work, hence the line under.
  document.getElementById('btnSendback').disabled = false;
  for(var i =0;i<$scope.model.modifieddetails.length;i++) {
    if($scope.model.modifieddetails[i].Seq == d.Seq) {
      found = true;
      $scope.model.modifieddetails[i].Units = d.Units;
      $scope.model.modifieddetails[i].Price = d.Price;
      console.log('Modificationlist: ', $scope.model.modifieddetails);
      return;
    }
  }
  if(!found) {
    $scope.model.modifieddetails.push(d);
  }
  console.log('Modificationlist: ', $scope.model.modifieddetails);
 }

 $scope.editArticle = function(o) {
    $ionicListDelegate.closeOptionButtons();
    $scope.data = o;

    $ionicPopup.show({
      templateUrl: 'templates/tab-order/popup-edit.html',
      title: o.CAB_DESC,
      subTitle: '',
      scope: $scope,
      buttons: [
        { 
          text: 'Annuleer' 
        },
        {
          text: '<b>Wijzigen</b>',
          type: 'button-positive',

          onTap: function(e) {
            // console.log('Price: ', o.ORDE_ITEM_PRICE);
            // console.log('Units: ', o.ORDE_NUM_OF_UNITS);
            // console.log('LineValue: ', o.ORDE_NUM_OF_UNITS*o.CACA_NUM_OF_UNITS*$scope.getValue(o))
            $scope.addModification({
              Seq: o.ORDE_SEQUENCE, 
              Units: o.ORDE_NUM_OF_UNITS, 
              Price: $scope.getValue(o)
            });
            return o.ORDE_ITEM_PRICE;
          }
        }
      ]
    });
  };  
});