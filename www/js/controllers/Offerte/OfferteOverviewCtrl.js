angular.module('LogiCAB.controllers')
.controller('OfferteOverviewCtrl', function($scope, $localstorage) {
  var offerte_data = $localstorage.getObject('current_offerte_data');
  $scope.data = offerte_data;
  console.log('Offerte_data:', offerte_data);
  $scope.ResetOfferte = function() {
    $localstorage.setObject('current_offerte_data', undefined);
    $scope.data = undefined;
  }
});