angular.module('LogiCAB.controllers')
.controller('OfferteBuyersCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, Offerte, $stateParams,$ionicListDelegate, $ionicPopup, $ionicHistory, $localstorage) {
  console.log('OfferteBuyersCtrl');
  var id = $stateParams.offerteId;
  $scope.model = {};
  $scope.model.id = id;
  $scope.model.offerte = Offerte.getByNr(id);

  Offerte.getBuyersList().then(function(buyers) {
    $scope.model.buyers = buyers;
    for(var i=0;i<$scope.model.buyers.length;i++) {
      $scope.model.buyers[i].checked = false;
    }
    var current_offerte_buyers = $localstorage.getObject('current_offerte_buyers');
    if(current_offerte_buyers != undefined) {
      for(var i=0;i<$scope.model.buyers.length;i++) {
        for(var y=0;y<current_offerte_buyers.length;y++) {
          if(current_offerte_buyers[y].OFHD_FK_BUYER == $scope.model.buyers[i].BuyerSequence) {
            console.log('Buyer found: ', $scope.model.buyers[i]);
            $scope.model.buyers[i].checked = true;
          }
        }        
      } 
    }    
  });  


  $scope.getBuyers = function() {
    var buyers = [];
    for(var i=0;i<$scope.model.buyers.length;i++) {
      if($scope.model.buyers[i].checked) buyers.push($scope.model.buyers[i]);
    }
    console.log('Buyers: ', buyers);
    return buyers;
  }
  var selectAll = true;
  $scope.selectAllBuyers = function() {
    for(var i=0;i<$scope.model.buyers.length;i++) {
      $scope.model.buyers[i].checked = selectAll;
    }    
    selectAll = !selectAll;
  }

  $scope.saveOffer = function(mail) {
    $ionicListDelegate.closeOptionButtons();
    $scope.data = {};
    

  
    var title = '';
    if(mail == 1) {
      title = 'Weet u zeker dat u de offerte wilt bewaren en mailen?';
    } else {
      title = 'Weet u zeker dat u de offerte wilt bewaren?';
    }
    var myPopup = $ionicPopup.show({
      template: '',
      title: title,
      subTitle: '',
      scope: $scope,
      buttons: [
        { text: '<b>Annuleer</b>' },
        {
          text: '<b>Ja</b>',
          type: 'button-dark',
          onTap: function(e) {
            var buyers = $scope.getBuyers();            
            Offerte.publish(buyers).then(function(ret) {
              var myPopupConfirm = $ionicPopup.show({
                    template: '',
                    title: 'Voltooid',
                    subTitle: '',
                    scope: $scope,
                    buttons: [
                      {
                        text: '<b>Oké!</b>',
                        type: 'button-dark',
                        onTap: function(e) {
                            $ionicHistory.nextViewOptions({
                              disableAnimate: true,
                              disableBack: true
                            });                        
                            $state.go('tab.offerte-list');
                            return 'Offerte opgeslagen!';
                        }
                      }
                    ]
                  });              
              });
              return 'Opgeslagen!';
          }
        }
      ]
    });
    myPopup.then(function(res) {
  //    console.log('Tapped!', res);
    });
 };  

  // Offerte.getById(id).then(function(offerterows) {
  //   console.log('stateParams.offerId: ', id);
  //   $scope.model.offerterows = offerterows;
  // })


});