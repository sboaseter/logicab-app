angular.module('LogiCAB.controllers')
.controller('OfferteDetailCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, Offerte, $stateParams, $localstorage, $ionicPopup) {
  console.log('OfferteDetailCtrl');
  var id = $stateParams.offerteId;
  $scope.model = {};
  $scope.model.id = id;
  $scope.model.offerte = Offerte.getByNr(id);

  $scope.model.newoffer = {
    Sequence: User.getGrower(),
    fromDate: $scope.model.offerte.OFHD_VALID_FROM,
    toDate: $scope.model.offerte.OFHD_VALID_TO,
    offertype: $scope.model.offerte.OFHD_OFFER_TYPE,
    items: [],
    buyers: undefined
  };
 

  Offerte.getById(id).then(function(offerterows) {
    console.log('stateParams.offerId: ', id);
    $scope.model.offerterows = offerterows;
    for(var i=0;i<offerterows.length;i++) {
      
      var row = offerterows[i];
      var nItem = {};
      if(typeof row.OFDT_ITEM_PRICE == "string" ) {
        nItem.offer_price = parseFloat(row.OFDT_ITEM_PRICE.replace(',', '.'));
      } else {
        nItem.offer_price = parseFloat(row.OFDT_ITEM_PRICE);
      }
      nItem.offer_units = parseInt(row.OFDT_NUM_OF_ITEMS);
      nItem.offer_special = row.IsSpecial;

      nItem.cabseq = row.OFDT_FK_CAB_CODE;
      nItem.CSEQ = row.OFDT_FK_CACA_SEQ;
      nItem.pictureseq = row.PIC_SEQ;
      nItem.seq = row.GRAS_SEQUENCE;

      nItem.desc = row.PROP_CAB_DESC;

      nItem.Properties = row.Properties;
      nItem.Cask = row.CDCA_CASK_CODE;
      nItem.numitems = row.GRAS_NUM_OF_ITEMS;
      nItem.CNOU = row.CACA_NUM_OF_UNITS;
      nItem.gseq = row.GRAS_SEQUENCE;

      nItem.Group = row.Group;
      nItem.Grouptype = row.Grouptype;
      nItem.Maingroup = row.Maingroup;

      nItem.offer_include = true;      
      $scope.model.newoffer.items.push(nItem);
    }
    $scope.saveOfferte();
    console.log('newoffer: ', $scope.model.newoffer);
  });

  Offerte.getBuyersByNo(id).then(function(buyerslist) {
    $scope.model.buyerslist = buyerslist;
    $localstorage.setObject('current_offerte_buyers', buyerslist);    
    console.log('Buyers: ', buyerslist);

  });

  $scope.isSpecial = function(spVal) {
    if(spVal) {
      return '<img src="img/special.png" />';
    }
    return '';
  }; 

  $scope.saveOfferte = function() {
    // Delete once offerte is sent!
    console.log('Saving offerte: ', $scope.model.newoffer);
    $localstorage.setObject('current_offerte_data', $scope.model.newoffer);    
  };
  $scope.copyOffer = function() {
    $state.go('tab.offerte-new');
  };

  $scope.editArticle = function(o) {
    console.log('Article to edit: ', o);
    $scope.data = {}
    $scope.data.OFDT_NUM_OF_ITEMS = o.OFDT_NUM_OF_ITEMS;
    $ionicPopup.show({
      templateUrl: 'templates/tab-offerte/popup-edit-existing.html',
      title: o.PROP_CAB_DESC,
      subTitle: '',
      scope: $scope,
      buttons: [
        { text: 'Annuleer' },
        {
          text: '<b>Wijzigen</b>',
          type: 'button-positive',

          onTap: function(e) {
            console.log('Article: ', o);
            o.OFDT_NUM_OF_ITEMS = $scope.data.OFDT_NUM_OF_ITEMS;
            console.log('Change commited: ', $scope.data.OFDT_NUM_OF_ITEMS);
            console.log('Calling OfferteSvc with OFDT_NUM_OF_ITEMS:', $scope.data.OFDT_NUM_OF_ITEMS, ' for: ', o);
            Offerte.modifyRow(o).then(function() {
              var myPopupConfirm = $ionicPopup.show({
                    template: '',
                    title: 'Voltooid',
                    subTitle: '',
                    scope: $scope,
                    buttons: [
                      {
                        text: '<b>Oké!</b>',
                        type: 'button-dark',
                        onTap: function(e) {
                        }
                      }
                    ]
                  });              
            });
          }
        }
      ]
    });
  }      
  

  $ionicModal.fromTemplateUrl('templates/tab-offerte/popup-buyers.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

});