angular.module('LogiCAB.controllers')
.controller('OfferteNewCtrl', function($scope, $rootScope, $state, User, $ionicLoading, apiUrls, $ionicModal, Offerte, $stateParams, Assortiment, $ionicListDelegate, $ionicPopup, $cordovaDatePicker, $ionicPopover, $localstorage, $localStorage, $cordovaFileTransfer, TourGuide) {
  console.log('OfferteNewCtrl');
  TourGuide.start($state.current.name, $scope);

  
  $scope.model = {};
  $scope.model.newoffer = {
    items: []
  };
  $scope.$storage = $localStorage; 

  Offerte.getOfferNo().then(function(nr) {
    $scope.model.offerno = nr;
    $scope.model.newoffer.nr = nr;
  })

  Assortiment.getList().then(function(assortiment) {
    $scope.model.assortiment = assortiment;
  });

  Offerte.getNextWorkingDay().then(function(data) {
        $scope.model.fromDate = moment(data).toDate();
        console.log('Next working day: ', $scope.model.fromDate);
        $scope.model.fromTime = 0;
        $scope.model.toDate = moment($scope.model.fromDate).add(1, 'day').hours(0).minutes(0).seconds(0).toDate();
        $scope.model.toTime = 60*60*6;

    $rootScope.tmpStorage.fromDate = $scope.model.fromDate;
    $rootScope.tmpStorage.toDate = $scope.model.toDate;        
  });


 

  $scope.model.offertype = "S"; // Redundant
  $scope.model.newoffer.offertype = "S";
  $scope.model.offerteTypeStock = true;
  $scope.model.offerteTypeOffer = false;

  $scope.saveOfferte = function() {
    // Delete once offerte is sent!
    $localstorage.setObject('current_offerte_data', {
      nr: $scope.model.newoffer.nr,
      fromDate: $scope.model.fromDate,
      fromTime: $scope.model.fromTime,
      toDate: $scope.model.toDate,
      toTime: $scope.model.toTime,
      offertype: $scope.model.newoffer.offertype,
      items: $scope.model.newoffer.items,
      buyers: undefined
    });    
  }

  var current_offerte_data = $localstorage.getObject('current_offerte_data');
  console.log('current_offerte_data: ', current_offerte_data);
  if(current_offerte_data != undefined) {
    console.log('New offerte in localstorage');
//    current_offerte_data.fromDate = new Date(current_offerte_data.fromDate);
//    current_offerte_data.toDate = new Date(current_offerte_data.toDate);
    $scope.model.newoffer = current_offerte_data;
    $scope.model.newoffer.fromDate = $scope.model.fromDate;
    $scope.model.newoffer.toDate = $scope.model.toDate;
    $scope.saveOfferte();
  }



  $scope.resetOfferte = function() {
    $localstorage.setObject('current_offerte_data', undefined);
    $scope.model.newoffer = {
      items: []
    };
  }  

  $scope.setOffertype = function(type) {
      $scope.model.offertype = type;
      $scope.model.newoffer.offertype = type;
      if(type == 'S') { $scope.model.offerteTypeStock = true; $scope.model.offerteTypeOffer = false; }
      if(type == 'O') { $scope.model.offerteTypeStock = false; $scope.model.offerteTypeOffer = true; }
  };

  $scope.model.article = {};

  $scope.showArticleInfo = function(a) {
    console.log(a);
    $scope.model.article = a;
    $scope.model.article.showArticle = true;

  }
  $scope.getArticle = function(a) {
    var cList = $scope.model.assortiment;
    for(var i=0;i<cList.length;i++) {
//      console.log(cList[i].gseq);
      if(cList[i].gseq == a.gseq) return cList[i];
    }    
  }


  $scope.isSpecial = function(spVal) {
    if(spVal) {
      return '<img src="img/special.png" style="max-width:50%; max-height:50%;" />';
    }
    return '';
  }

  $scope.showCollectedData = function() {
    $scope.saveOfferte();
    $state.go('tab.offerte-overview');
  }

  $scope.editArticle = function(a) {
    console.log('a: ', a);
    console.log('assortiment.length: ', $scope.model.assortiment.length);
    var lsArticle = $scope.getArticle(a);
    console.log('lsArticle: ', lsArticle);
    $ionicListDelegate.closeOptionButtons();
    $scope.data = {}

    var editYN = false;
    if($scope.model.newoffer.items.length >= 1 ) {
      for(var i=0;i<$scope.model.newoffer.items.length;i++) {
        if($scope.model.newoffer.items[i].gseq == a.gseq) {
          var fItem = $scope.model.newoffer.items[i];
          $scope.data.price = fItem.offer_price;
          $scope.data.num_to_offer = parseInt(fItem.offer_units);
          $scope.data.remark = fItem.offer_remark;
          $scope.data.special = fItem.offer_special;
          editYN = true;
          break;
        }
      }
    }

    $ionicPopup.show({
      templateUrl: 'templates/tab-offerte/popup-edit.html',
      title: a.desc,
      subTitle: '',
      scope: $scope,
      buttons: [
        { text: 'Annuleer' },
        {
          text: editYN ? '<b>Wijzigen</b>' : '<b>Toevoegen</b>',
          type: 'button-positive',

          onTap: function(e) {
/*
  Determine if article allready exists in $scope.model.newoffer.items, if so, edit, if not add!

  Add method to take an existing offer and fill a $scope.model.newoffer with the data. Also collect buyersequences?

*/
            var foundAndEdited = false;
            console.log('current.scope: ', $scope);
            if($scope.model.newoffer.items.length >= 1 ) {
              for(var i=0;i<$scope.model.newoffer.items.length;i++) {
                if($scope.model.newoffer.items[i].gseq == a.gseq) {
                  if(typeof $scope.data.price == "string" ) {
                    $scope.model.newoffer.items[i].offer_price = parseFloat($scope.data.price.replace(',', '.'));
                  } else {
                    $scope.model.newoffer.items[i].offer_price = parseFloat($scope.data.price);
                  }
                  $scope.model.newoffer.items[i].offer_units = parseInt($scope.data.num_to_offer);
                  $scope.model.newoffer.items[i].offer_remark = $scope.data.remark;
                  $scope.model.newoffer.items[i].offer_special = $scope.data.special;
                  $scope.model.newoffer.items[i].offer_include = true;
                  foundAndEdited = true;
                  break;
                }
              }
            }
            if(!foundAndEdited) {
              lsArticle.offer_price = parseFloat($scope.data.price.replace(',', '.'));
              lsArticle.offer_units = parseInt($scope.data.num_to_offer);
              lsArticle.offer_remark = $scope.data.remark;
              lsArticle.offer_special = $scope.data.special;
              lsArticle.offer_include = true;
              $scope.model.newoffer.items.push(lsArticle);
            }            
            // for(var i=0;i<$scope.model.assortiment.length;i++) {
            //   if($scope.model.assortiment[i].gseq == lsArticle.gseq) {
            //     $scope.model.assortiment[i].assort_include = false;
            //   }
            // }
            $scope.saveOfferte(); 
            return [$scope.data.num_to_order, parseFloat($scope.data.price)];
          }
        }
      ]
    });
  }  

  $scope.deleteArticle = function(a) {   
    console.log
    $ionicPopup.show({
      template: '<div class="text-center" style="color: white;font-weight: bold;">' + a.desc + ' verwijderen?</div>',
      title: 'Artikel verwijderen',
      subTitle: '',
      scope: $scope,
      buttons: [
        { 
          text: 'Annuleer',
          onTap: function(e) {
            return;
          }
        },
        {
          text: '<b>Verwijderen</b>',
          type: 'button-positive',

          onTap: function(e) {
            // Modify NewOffer
            for(var i=0;i<$scope.model.newoffer.items.length;i++) {
              if($scope.model.newoffer.items[i].gseq == a.gseq) {
                $scope.model.newoffer.items.splice(i, 1);
              }
            } 
            // Modify Assortiment-list
            for(var i=0;i<$scope.model.assortiment.length;i++) {
              if($scope.model.assortiment[i].gseq == a.gseq) {
                $scope.model.assortiment[i].assort_include = true;
                $scope.model.assortiment[i].offer_price = 0;
                
              }
            }           
          }
        }
      ]
    });                
    $scope.saveOfferte();  
  }  

  // Redundant
  $scope.getArticlesToPublish = function() {
    var cList = $localstorage.getObject($scope.model.offerno + "_newoffer");
    var pList = [];
    for(var i=0;i<cList.length;i++) {
      if(cList[i].offer_price != undefined && cList[i].offer_include == true) {
        pList.push(cList[i]);
      }
    }
    console.log('To publish: ');
    console.log(pList);
  };
  $scope.testClick = function(a) {
    console.log('ArtSeq: ', a);
//     Camera.getPicture().then(function(fileURI) {
//      console.log(imageURI.length);
      //$localstorage.setObject('cpic_'+a, imageURI);
//       Camera.sendPicture(imageURI).then(function(res) {
//        console.log('sendPicture result: ', res);
////        $scope.replaceImage(a, res);
//       });
       var options = {
         quality: 50,
         destinationType : Camera.DestinationType.FILE_URI,
         sourceType : Camera.PictureSourceType.CAMERA, // Camera.PictureSourceType.PHOTOLIBRARY
         allowEdit : false,
         encodingType: Camera.EncodingType.JPEG,
         popoverOptions: CameraPopoverOptions,
       }; 

      navigator.camera.getPicture(function(imageURI) { 
        console.log('getPicture url: ', imageURI);
      var url = "http://apidev.logicab.nl/api/v1/Echo/uploadfile";
//      var file = fileURI.substr(fileURI.lastIndexOf('/')+1);
      var optionsU = {};
//      var img = document.createElement("img");
//      img.src = window.webkitURL.createObjectURL(imageURI);

var img = document.createElement("img");
img.src = imageURI;
console.log('img.src: ', img.src);
//var reader = new FileReader();
//reader.onload = function(e) {img.src = e.target.result}
//reader.readAsDataURL(imageURI);


var MAX_WIDTH = 800;
var MAX_HEIGHT = 600;
var width = img.width;
var height = img.height;
 
if (width > height) {
  if (width > MAX_WIDTH) {
    height *= MAX_WIDTH / width;
    width = MAX_WIDTH;
  }
} else {
  if (height > MAX_HEIGHT) {
    width *= MAX_HEIGHT / height;
    height = MAX_HEIGHT;
  }
}
var canvas = document.createElement('canvas');
canvas.width = width;
canvas.height = height;
var ctx = canvas.getContext("2d");
ctx.drawImage(img, 0, 0, width, height);
console.log('Image drawn on canves!');
var dataurl = canvas.toDataURL("image/png");
console.log('dataurl: ', dataurl);
//var nFile = $scope.saveToFile(ctx,'newfile.jpg', 'jpeg', {});
//var file = canvas.mozGetAsFile(imageURI);


        $cordovaFileTransfer.upload(url, imageURI, optionsU)
          .then(function(result) {
            console.log('Success upload: ', JSON.stringify(result));
            $scope.replaceImage(a, result.response);

            // Success!
          }, function(err) {
            console.log('Error upload: ', JSON.stringify(err));
            // Error
          }, function (progress) {
            // constant progress updates
            console.log('Progress upload: ', progress);
          });
      }, function(message) {
        console.log('Error occured')
      }, options);




//    }, function(err) {
//      console.err(err);
//    });
  };



  $scope.replaceImage = function(a,pseq) {
    for(var i=0;i<$scope.model.newoffer.items.length;i++) {
      if($scope.model.newoffer.items[i].pictureseq == a) {
        console.log('Found correct article, replace pictureseq to: ', pseq);
        $scope.model.newoffer.items[i].pictureseq = pseq;
        $scope.model.newoffer.items[i].artpictureseq = pseq;        
        break;
      }      
    }
    $scope.saveOfferte();
  };
//  $scope.imgCameraUrl = function(seq) {
//    console.log('imgCameraUrl: ', $scope['photourl_' + seq]);
//    return $scope['photourl_' + seq];
  //};
  $scope.getImgUrl = function(seq) {
    //var art = $scope.getArticle(seq);
//    console.log('Art: ', art);
    var url = $rootScope.imgUrl(seq);
//    console.log('getImgUrl: ', url);
//    var curl = $localstorage.getObject('cpic_'+seq);
//    console.log('getImgUrl url: ', url);
//    console.log('getImgUrl curl: ', curl);
  //  if(curl != null) {
//      return curl;
//    } 
//    if(curl != null && curl.length > 1) {
//      console.log('Returning Curl.');
//      return curl;
//    }
    return url;

  };

  $scope.$watchGroup(['model.fromDate', 'model.toDate', 'model.fromTime', 'model.toTime'], function(newValues, oldValues, scope) {
    // newValues array contains the current values of the watch expressions
    // with the indexes matching those of the watchExpression array
    // i.e.
    // newValues[0] -> $scope.foo 
    // and 
    // newValues[1] -> $scope.bar
//    console.log('watchGroup oldValues: ', oldValues);
    console.log('watchGroup newValues: ', newValues);
    console.log('model.fromDate: ', $scope.model.fromDate);
    $scope.saveOfferte();
//    $scope.$storage.fromDate = newValues[0].getTime();
    $rootScope.tmpStorage.fromDate = newValues[0];
    $rootScope.tmpStorage.toDate = newValues[1];
//    $scope.$storage.toDate = newValues[1].getTime();
    $scope.$storage.fromTime = newValues[2];
    $scope.$storage.toTime = newValues[3];
    console.log('$rootScope.tmpStorage.fromDate: ', $rootScope.tmpStorage.fromDate);
  });  

});