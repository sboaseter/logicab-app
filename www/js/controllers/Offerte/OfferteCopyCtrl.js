angular.module('LogiCAB.controllers')
.controller('OfferteCopyCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, Offerte, $stateParams, Assortiment, $ionicListDelegate, $ionicPopup) {
  console.log('OfferteCopyCtrl');
  var id = $stateParams.offerteId;
  $scope.model = {};
  $scope.model.id = id;
  $scope.model.offerte = Offerte.getByNr(id);

  Offerte.getById(id).then(function(offerterows) {
    console.log('stateParams.offerId: ', id);
    $scope.model.offerterows = offerterows;
    Offerte.saveOfferToLocal(offerterows);
  });
  Assortiment.getList().then(function(assortiment) {
    $scope.model.assortiment = assortiment;

  });
$scope.addArticleToOffer = function(article) {
  $ionicListDelegate.closeOptionButtons();
  $scope.data = {}
  console.log('AssortimentSequence: ', article);
  // An elaborate, custom popup
  var myPopup = $ionicPopup.show({
    template: '<input type="tel" ng-model="data.num_to_offer" placeholder="Aantal" style="border-bottom: 1px solid grey;"><input type="tel" ng-model="data.price" placeholder="Prijs">',
    title: article.prop,
    subTitle: '',
    scope: $scope,
    buttons: [
      { text: 'Annuleer' },
      {
        text: '<b>Toevoegen</b>',
        type: 'button-positive',
        onTap: function(e) {
          console.log('Num to offer: ', $scope.data.num_to_offer);
          console.log('Price pr item: ', $scope.data.price);
          console.log('From article: ', article);
          Offerte.addToOffer(article);
          return $scope.data.num_to_order;
        }
      }
    ]
  });
  myPopup.then(function(res) {
//    console.log('Tapped!', res);
  });
 };
 $scope.adjustPrice = function(article) {
  $ionicListDelegate.closeOptionButtons();
  $scope.data = {}
  console.log('AssortimentSequence: ', article);
  $scope.data.price = article.OFDT_ITEM_PRICE;
  // An elaborate, custom popup
  var myPopup = $ionicPopup.show({
    template: '<input type="tel" ng-model="data.price">',
    title: article.PROP_CAB_DESC + '<br />' + article.Properties + '<br />Price',
    subTitle: '',
    scope: $scope,
    buttons: [
      { text: 'Cancel' },
      {
        text: '<b>Ok</b>',
        type: 'button-positive',
        onTap: function(e) {
          console.log('Price: ', $scope.data.price);
          console.log('From article: ', article);
//          Offerte.addToOffer(article);
          return $scope.data.price;
        }
      }
    ]
  });
  myPopup.then(function(res) {
//    console.log('Tapped!', res);
  });  
 }
});