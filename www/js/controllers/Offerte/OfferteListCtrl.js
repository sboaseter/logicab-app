angular.module('LogiCAB.controllers')
.controller('OfferteListCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, Offerte) {
  console.log('OfferteListCtrl');
  $scope.model = {};

  Offerte.getList(User.getGrower()).then(function(offertelist) {
    $scope.offertes = offertelist;
  });

  $scope.model.msg = "Hi";
  $scope.isValid = function(cdate) {
      var date = moment(cdate)
    var now = moment();

  //  console.log(date);

    if (now < date) {
       // date is past
       return 'actueel';
    } else {
       // date is future
       return 'verlopen';
    }
  }
  $scope.isValidColor = function(cdate) {
      var date = moment(cdate)
    var now = moment();

//    console.log(date);

    if (now < date) {
       // date is past
       return 'badge-balanced';
    } else {
       // date is future
       return 'badge-assertive';
    }
  }  
  $scope.doRefresh = function() {
    console.log('Refreshing...');
    Offerte.getList(User.getGrower()).then(function(offertelist) {
      $scope.offertes = offertelist;
      console.log('Refresh complete.');
      $scope.$broadcast('scroll.refreshComplete');    
    });
  } 
  $scope.deleteOfferte = function(o) {
    var nr = o.OFHD_NO;
    Offerte.deleteOfferte(nr).then(function(data) {
      $scope.doRefresh();
    });
  }; 
});