angular.module('LogiCAB.controllers')
.controller('OfferteCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, TourGuide) {
	console.log('OfferteCtrl', $state.current.name);
	TourGuide.start($state.current.name, $scope);
	
  	$scope.model = {};
  	$scope.model.msg = "Hi";

//  	$scope.model.maingroups = [{id: 1, name: "plants", checked: false}, {id: 2, name: "flowers", checked: true}];
	$scope.model.maingroups = [];
	var smg = User.getSelectedMaingroup();
  	User.getMaingroups().then(function(res) {
  		for(var i=0;i<res.length;i++) {
  			if(res[i].Sequence == smg) {
  				res[i].checked = true;
  			} else {
  				res[i].checked = false;
  			}
  		}
  		$scope.model.maingroups = res;
  		console.log('Maingroups fetched: ', res);
  	});


  	$scope.setSelectedMaingroup = function(mg) {
  		console.log('New maingroup selected: ', mg);
  		User.setSelectedMaingroup(mg);
	    for(var i=0;i<$scope.model.maingroups.length;i++) {
	    	if($scope.model.maingroups[i].Sequence == mg) {
	    		$scope.model.maingroups[i].checked = true;	      	
	    	} else {
	    		$scope.model.maingroups[i].checked = false;
	    	}
	  	}
  	};
});