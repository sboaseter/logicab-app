angular.module('LogiCAB.controllers')
.controller('OfferCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, Order, Offers) {
  $scope.model = {};
  $scope.model.msg = "Welcome to Offers";

  var buyer = User.getBuyer();
    Offers.getall(buyer).then(function(offers) {
      var l = Offers.getMaingroups();
      $scope.offers = l;
      //$scope.offers.push(l);
      console.log('res: ', $scope.offers);     
    });


  $scope.deliveryDateImage = function(offer) {
//    console.log('Offer: ', offer);
    if(offer.Maingroup == 2) {
      return "img/flower.png";
    } else if(offer.Maingroup == 1) {
      return "img/plant.png";
    } else if(offer.Maingroup == 4) {
      return "img/misc.png";
    }
  }
/* $scope.doRefresh = function() {
  console.log('Refreshing...');
  var buyer = User.getBuyer();
    Offers.getall(buyer).then(function(offers) {
      var l = Offers.getMaingroups();
      $scope.offers = l;
      console.log('Refresh complete.');
       $scope.$broadcast('scroll.refreshComplete');
    });  
  }  */
});