angular.module('LogiCAB.controllers')
.controller('CartCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, Order, Cart,$rootScope) {

  console.log('CartCtrl');

  $scope.model = {};
  $scope.model.msg = "Welcome to cart!";

  $scope.emptyCart = Cart.isEmpty();
  $scope.confirmed = Cart.isConfirmed();
  $scope.showCart = !$scope.emptyCart; // && !$scope.confirmed;

  $scope.cart = Cart.getCart();

  
  $scope.cartValue = Cart.getTotalValue();
  $scope.selectedDate = $rootScope.formatDateDay(Cart.getSelectedDate());
  


 $scope.showAlert = function() {
   var alertPopup = $ionicPopup.alert({
     title: 'Confirming...',
     template: 'Order is confirming, please wait...'
   });
   alertPopup.then(function(res) {
     console.log('Thank you for not eating my delicious ice cream cone');
   });
 }; 

  $scope.confirmOrder = function() {

    console.log('Confirm order: value: ', $scope.cartValue, ' Content: ', $scope.cart);
//    $scope.showAlert();
    $ionicLoading.show();
    Cart.confirmOrder().then(function() {
      $scope.confirmed = true;
      $ionicLoading.hide();
      Cart.resetCart();
      $rootScope.cartCount();
//      $scope.showAlert();
    });
  }
  $scope.backToOffer = function() {
    //cleanup
    Cart.resetCart();

    $state.go('tab.offer');
  }
  $scope.removeFromCart = function(orderdetail) {
    Cart.removeFromCart(orderdetail);
  $scope.cart = Cart.getCart();
  $scope.cartValue = Cart.getTotalValue();    

  }

  $scope.addOneUnit = function(orderdetail) {
    Cart.addOneUnit(orderdetail);
  $scope.cart = Cart.getCart();
  $scope.cartValue = Cart.getTotalValue();        
  }

});