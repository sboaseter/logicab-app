angular.module('LogiCAB.controllers')
.controller('BuyerOrderCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, Order, TourGuide) {
  $scope.model = {};
  $scope.model.msg = "Welcome to Orders";
  TourGuide.start($state.current.name, $scope);


	$scope.doRefresh = function() {
		console.log('Refreshing...');
	  	Order.listOrdersBuyer().then(function(orders) {
	  	$scope.model.orders = $scope.groupOrders(orders);
	  	console.log('BuyerOrders: ', $scope.model.orders);
	  	$scope.$broadcast('scroll.refreshComplete');  	  	
	  });    
	};
	Order.listOrdersBuyer().then(function(orders) {
		$scope.model.orders = $scope.groupOrders(orders);
		console.log('BuyerOrders: ', $scope.model.orders);
	});

  /* Fill the buckets */
	$scope.groupOrders = function(orders) {
		var Orderlist = [];
		for(var i=0;i<orders.length;i++) {
			var found = false;
			for(var y =0;y<Orderlist.length;y++) {
				if(Orderlist[y].OrgaSeq == orders[i].OrgaSeq) {
					found = true;
					Orderlist[y].Orders.push(orders[i]);
				}
			}
			if(!found) {
				Orderlist.push({
					OrgaSeq: orders[i].OrgaSeq,
					Orga: orders[i].Orga,
					Logo: orders[i].Logo,
					Orders: [orders[i]]
				});
			}
		}
		return Orderlist;
	}

  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };  
  $scope.isValidColor = function(cdate) {
     return 'badge-assertive';
  };
  $scope.getStatusTextColor = function(status, t) {
    var statusList = [
      "NA",
      "REQ",
      "CONFIRMED",
      "REJECTED",
      "KWBSENT",
      "EKTKWBSENT",
      "NEW",
      "CANCEL",
      "CHANGEDORDER",
      "CHANGEDBYGROWER",
      "ORDERED",
      "CHANGEDBYBUYER",
      "SAVEDBYBUYER",
      "SAVEDBYGROWER",
      "EKTSENT", 
      "BUYERRECALL"
    ];
    var statusListC = [
      "badge-stable",
      "badge-positive",
      "badge-balanced",
      "badge-assertive",
      "badge-balanced",
      "badge-balanced",
      "badge-positive",
      "badge-assertive",
      "badge-royal",
      "badge-royal",
      "badge-positive",
      "badge-royal",
      "badge-royal",
      "badge-royal",
      "badge-balanced", 
      "badge-royal"
    ];    
    if(t==0) {
      return statusList[status];
    }
    if(t==1) {
      return statusListC[status]; 
    }
  };  
});