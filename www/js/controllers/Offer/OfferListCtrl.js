angular.module('LogiCAB.controllers')
.controller('OfferListCtrl', function($scope, $stateParams, Offers, User, $ionicPopup, Order, Cart, $localstorage) {
  // Triggered by ion-option 'Voeg toe'
  $scope.showOrderNum = function(offerrow) {
    $scope.data = {}
    console.log('OfferSequence: ', offerrow);
    var myPopup = $ionicPopup.show({
      templateUrl: 'templates/tab-offer/popup-edit.html',
      title: 'Toevoegen aan winkelwagen',
      subTitle: '',
      scope: $scope,
      buttons: [
        { text: 'Annuleren' },
        {
          text: '<b>Voeg toe</b>',
          type: 'button-positive',
          onTap: function(e) {

            if(Cart.addToCart(offerrow, $scope.data.num_to_order, $scope.data.custcode, $scope.data.sticker)) {
              console.log('added');
            } else {
              $scope.showAlert(); // Verify error message at this point, insufficient stock? network error? other?
            }
            return $scope.data.num_to_order;
          }
        }
      ]
    });
    $scope.showAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'Date error',
       template: 'Selected Date: <br />' + $rootScope.formatDateDay(User.getSelectedDate()) + '<br />' + 'Cart: <br />' + $rootScope.formatDateDay(Order.getSelectedDate())
     });
    };   
  };


  var selectedDate = $stateParams.selectedDate;
  $scope.selectedDate = selectedDate;
  User.setSelectedDate(selectedDate);

  Offers.getList().then(function(list) {
    $scope.offerdetails = list;
    $localstorage.setObject('current_offerlist', list);
  });

  $scope.doRefresh = function() {
    Offers.getList().then(function(list) {
      $scope.offerdetails = list;
      $localstorage.setObject('current_offerlist', list);
      $scope.$broadcast('scroll.refreshComplete');      
    });
  };    
});
