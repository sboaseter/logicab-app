angular.module('LogiCAB.controllers')
.controller('OfferDatesCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, Order, Offers, $stateParams) {
  $scope.model = {};
  $scope.model.msg = "Welcome to Offers";

  var mg = $stateParams.mgId;
  $scope.mg = mg;
  User.setSelectedMaingroup(mg);
  $scope.deliverydates = Offers.getDates(mg);

  $scope.deliveryDateImage = function(id) {
    if(id == 2) {
      return "img/flower.png";
    } else if(id == 1) {
      return "img/plant.png";
    } else if(id == 4) {
      return "img/misc.png";
    }
  };
});