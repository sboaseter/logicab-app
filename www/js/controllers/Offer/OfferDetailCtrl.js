angular.module('LogiCAB.controllers')
.controller('OfferDetailCtrl', function($scope, $stateParams, Offers, User, $ionicPopup, Order, Cart, $ionicHistory, $localstorage, $ionicHistory) {

	var art = $stateParams.ofdt;
	console.log('Article: ', art);

	var item = {};
	var list = $localstorage.getObject('current_offerlist');
	for(var i=0;i<list.length;i++) {
		if(list[i].S == art) {
			item = list[i];
			console.log('item: ', item);
			break;
		}
	}
	$scope.model = {};
	$scope.model = item;

	$scope.showOrderNum = function(offerrow) {
		$scope.data = {}
		var myPopup = $ionicPopup.show({
		templateUrl: 'templates/tab-offer/popup-edit.html',
		title: 'Toevoegen aan winkelwagen',
		subTitle: '',
		scope: $scope,
		buttons: [
		  { text: 'Annuleren' },
		  {
		    text: '<b>Voeg toe</b>',
		    type: 'button-positive',
		    onTap: function(e) {

		      if(Cart.addToCart(offerrow, $scope.data.num_to_order, $scope.data.custcode, $scope.data.sticker)) {
		        console.log('added');
		      } else {
		        $scope.showAlert();
		      }
		    
				$ionicHistory.goBack();
		    }
		  }
		]
	});

	$scope.showAlert = function() {
		var alertPopup = $ionicPopup.alert({
		 title: 'Date error',
		 template: 'Selected Date: <br />' + $rootScope.formatDateDay(User.getSelectedDate()) + '<br />' + 'Cart: <br />' + $rootScope.formatDateDay(Order.getSelectedDate())
		});
	};  
 };  
});
