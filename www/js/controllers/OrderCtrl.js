angular.module('LogiCAB.controllers')
.controller('OrderCtrl', function($scope, $state, User, $ionicLoading, apiUrls, $ionicModal, Order, TourGuide, $localstorage) {

  $scope.model = {};

  TourGuide.start($state.current.name, $scope);

	$scope.doRefresh = function() {
		console.log('Refreshing...');
	  	Order.listOrders().then(function(orders) {
	  	$scope.model.orders = $scope.groupOrders(orders);
      $localstorage.setObject('orderlist', $scope.model.orders);
	  	$scope.$broadcast('scroll.refreshComplete');  	  	
	  });    
	};

	Order.listOrders().then(function(orders) {
		$scope.model.orders = $scope.groupOrders(orders);
    $localstorage.setObject('orderlist', $scope.model.orders);
    console.log('Orders:', $scope.model.orders);    
	});

  /* Fill the buckets */
	$scope.groupOrders = function(orders) {
		var Orderlist = [];
		for(var i=0;i<orders.length;i++) {
			var found = false;
			for(var y =0;y<Orderlist.length;y++) {
				if(Orderlist[y].OrgaSeq == orders[i].OrgaSeq) {
					found = true;
					Orderlist[y].Orders.push(orders[i]);
				}
			}
			if(!found) {
				Orderlist.push({
					OrgaSeq: orders[i].OrgaSeq,
					Orga: orders[i].Orga,
					Logo: orders[i].Logo,
					Orders: [orders[i]]
				});
			}
		}
    for(var i=0;i<Orderlist.length;i++) {
      Orderlist[i].NumOrdered = 0;
      Orderlist[i].NumEKTSend = 0;
      Orderlist[i].NumKWBSend = 0;
      Orderlist[i].NumChanged = 0;
      for(var y=0;y<Orderlist[i].Orders.length;y++) {
        if(Orderlist[i].Orders[y].Status == 10) {
          Orderlist[i].NumOrdered = Orderlist[i].NumOrdered + 1;
        }
        if(Orderlist[i].Orders[y].Status == 4) {
          Orderlist[i].NumEKTSend = Orderlist[i].NumEKTSend + 1;
        }        
        if(Orderlist[i].Orders[y].Status == 14) {
          Orderlist[i].NumKWBSend = Orderlist[i].NumKWBSend + 1;
        }        
        if(Orderlist[i].Orders[y].Status == 9) {
          Orderlist[i].NumChanged = Orderlist[i].NumChanged + 1;
        }                
      }
    }
    console.log('Orderlist: ', Orderlist);
		return Orderlist;
	}

  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };  
  $scope.isValidColor = function(cdate) {
     return 'badge-assertive';
  };  
  
  $scope.ektStatusColor = function(status) {
  	if(status == 14 || status == 5 ) return "green";
  	return "red";
  };
  $scope.kwbStatusColor = function(status) {
  	if(status == 4 || status == 5 ) return "green";
  	return "red";
  };  
});