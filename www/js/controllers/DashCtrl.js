angular.module('LogiCAB.controllers')
.controller('DashCtrl', function(
    // Modules
    $scope, 
    $state, 
    $ionicLoading, 
    $ionicSlideBoxDelegate,
    // Services
    Offers, 
    User, 
    Order,
    CAB,
    Plaza,
    tourGuide) {
  console.log('From DashCtrl in seperate file!');
  console.log('tourGuide: ', tourGuide[0]);
  CAB.getNew().then(function(cabcodes) {
    $scope.newcabcodes = cabcodes;
    $ionicSlideBoxDelegate.update();
  });
  Plaza.getNews().then(function(news) {
    $scope.news = news;
  });

  $scope.personName = User.getName();

  $scope.userinformation = {
    "name" : User.getName(),
    "buyer": User.getBuyer()
  }


  $scope.doRefresh = function() {
      console.log('Refreshing...');
      CAB.getNew().then(function(cabcodes) {
        $scope.newcabcodes = cabcodes;
        $ionicSlideBoxDelegate.update();
      });
      Plaza.getNews().then(function(news) {
        $scope.news = news;
      });
      $scope.$broadcast('scroll.refreshComplete');    
  }

})
.controller('DashNewsCtrl', function(
    // Modules
    $scope, 
    $state, 
    $ionicLoading, 
    $ionicSlideBoxDelegate,
    $stateParams,
    // Services
    Plaza) {
    console.log($stateParams.newsId);
    var id = $stateParams.newsId;
    $scope.news = Plaza.getById(id);
    if($scope.news == undefined) $state.go('tab.dash');
    console.log($scope.news);
});