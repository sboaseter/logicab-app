angular.module('LogiCAB.services')
.factory('Offers', function($http, User) {
  var offers = [];
  var offerdetails = []; // localstorage
  var selectedMaingroup = 2;
  var selectedDate = null;
  var apiBaseUrl = User.getEnviroment().url;
  console.log('LogiOfferte.services.Offers initialized. apiBaseUrl: ', apiBaseUrl);  
  return {
    all: function() {
      return offers;
    },
    getMaingroups: function() {
      var ret = [];
      for(var i =0;i<offers.length;i++) {
        var item = {
          Maingroup: offers[i].MAIN_GROUP, 
          Num: offers[i].Nr_Det,
          NumDates: 1
        };
        var found = false;
        for(var y=0;y<ret.length;y++) {
          if(ret[y].Maingroup == item.Maingroup) {
            ret[y].NumDates = ret[y].NumDates+1;
            console.log('MG allready exists, continue');
            found = true;
          }
        }
        if(!found) {
          ret.push(item);              
        }
      }
      return ret;
    },
    getDates: function(maingroup) {
      var ret = [];
      for(var i=0;i<offers.length;i++) {
        if(offers[i].MAIN_GROUP == maingroup) ret.push(offers[i]);
      }
      return ret;

    },
    getall: function(buyer) {
      var apiUrl =  apiBaseUrl +'/Offer/deliverydates/' + buyer;  
      return $http.get(apiUrl).then(function(response) {
        offers = response.data;
        return offers;
      });
    },
    getList: function() {
      var ret = false;
      var choices = User.getChoices();
      console.log('getList: choices: ', choices);
      var apiUrl =  apiBaseUrl +'/Offer/list';  
      return $http.post(apiUrl, choices).then(function(response) {
        this.offerdetails = response.data;
        console.log(response.data);
        return this.offerdetails;
      });
    },
    getById: function(id) {
      // Adjust to use localstorage
      var choices = User.getChoices();
      var apiUrl =  apiBaseUrl +'/Offer/list';  
      return $http.post(apiUrl, choices).then(function(response) {
        var data = response.data;
        for(var i=0;i<data.length;i++) {
          if(data[i].S == id) {
            return data[i];
          }
        }
      });      
    }
  }
});