angular.module('LogiCAB.services')
.factory('Order', function(
    $state, 
    $http, 
    $localstorage, 
    Offers, 
    User) {
  
  var currentOrderlist = null;
  var apiBaseUrl = User.getEnviroment().url;
  console.log('LogiOfferte.services.Order initialized. apiBaseUrl: ', apiBaseUrl);
  return {
    listOrders: function() {
      var apiUrl =  apiBaseUrl +'/Order/list';    
      var user = User.getUser(); 
      var settings = User.getSettings();
      var fromDate = new Date(); // Candidate for Settings-page
      fromDate.setDate(fromDate.getDate()-settings.historyDays);
      fromDate = moment(fromDate).format();
      var data = {
        Grower: user.grower,
        FromDate: fromDate
      };
      return $http.post(apiUrl, data).then(function(response) {
        currentOrderlist = response.data;
        return response.data;
      });      
    },
    listOrdersBuyer: function() {
      var apiUrl =  apiBaseUrl +'/Order/list';    
      var user = User.getUser(); 
      var fromDate = new Date(); // Candidate for Settings-page
      fromDate.setDate(fromDate.getDate()-64);
      fromDate = moment(fromDate).format();
      var data = {
        Buyer: user.buyer,
        FromDate: fromDate
      };
      return $http.post(apiUrl, data).then(function(response) {
        currentOrderlist = response.data;
        return response.data;
      });      
    },    
    listDetail: function(id) {
      var apiUrl = User.getEnviroment().url + '/Order/details/' + id;
      return $http.get(apiUrl).then(function(response) {
        console.log(response.data);
        return response.data;
      });
    },
    getById: function(id) {
      var ret = null;
      if(currentOrderlist == null) return null;
      for(var i=0;i<currentOrderlist.length;i++) {
        if(currentOrderlist[i].Seq == id) return currentOrderlist[i];
      }
      return null;
    },
    modifyOrder: function(data) {
      var apiUrl =  apiBaseUrl +'/Order/modify';  
      return $http.post(apiUrl, data).then(function(response) {
        return response.data;
      });
    },
    acceptOrder: function(id) {
      console.log('Accepting order: ', id);
      var apiUrl =  apiBaseUrl +'/Order/accept/' + id;    
      return $http.get(apiUrl).then(function(response) {
        return response.data;
      });        
    },
    sendEKT: function(id) {
      console.log('Sending EKT for order: ', id);
      var apiUrl =  apiBaseUrl +'/Order/sendekt/' + id;    
      return $http.get(apiUrl).then(function(response) {
        return response.data;
      });        
    },
    sendKWB: function(id) {
      console.log('Sending KWB for order: ', id);
      var apiUrl =  apiBaseUrl +'/Order/sendkwb/' + id;    
      return $http.get(apiUrl).then(function(response) {
        return response.data;
      });   
    }
  }
});