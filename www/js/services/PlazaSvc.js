angular.module('LogiCAB.services')
.factory('Plaza', function($http, User) {
  var news = {};
  return {
    getNews: function() {
      var apiUrl = User.getEnviroment().url + '/News/list';
      return $http.get(apiUrl).then(function(response) {
        console.log(response.data);
        news = response.data;
        return response.data;
      })
    },
    getById: function(id) {
      for(var i=0;i<news.length;i++) {
        if(news[i].Sequence == id) return news[i];
      }
    }
  };
});