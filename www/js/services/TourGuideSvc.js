angular.module('LogiCAB.services')
.factory('TourGuide', function($http, User, $ionicModal) {
  var apiBaseUrl = User.getEnviroment().url;
  var stateName = '';
  var t = '';
  var tc = 0;
  var scope = {};
  return {    
    setSeen: function() {
      scope.modal.hide()
      User.setTourVisited(stateName);
    },
    next: function() {
      scope.modal.hide()
      if(tc == t.length) { 
        User.setTourVisited(stateName);
        t = User.getTour(t);
        scope.modal.hide(); 
      } else {
        $ionicModal.fromTemplateUrl(t[tc++], {
            scope: scope,
            animation: 'none'
          }).then(function(modal) {
            scope.modal = modal;
            scope.modal.show();
          });
      }      
    },
    prev: function() {
      scope.modal.hide()
      if(tc < 0) { 
        scope.modal.hide(); 
      } else {
        --tc;
        $ionicModal.fromTemplateUrl(t[tc-1], {
            scope: scope,
            animation: 'none'
          }).then(function(modal) {
            scope.modal = modal;
            scope.modal.show();
          });
      }
    },
    start: function(state, iscope) {
      scope = iscope;
      stateName = state;
      t = User.getTour(state); 
      tc = 0;      
      if(t != '') {
        $ionicModal.fromTemplateUrl(t[tc++], {
          scope: scope,
          animation: 'none'
        }).then(function(modal) {
          scope.modal = modal;
          scope.modal.show();
        });
      }      
    }
  };
});