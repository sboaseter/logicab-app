angular.module('LogiCAB.services')
.factory('Offerte', function($http, User, $localstorage, $localStorage, $rootScope) {
  var apiBaseUrl = User.getEnviroment().url;
  var currentOfferteList = null;
  return {
    getBuyersById: function(id) {
      return ["koper1", "koper2", "koper3"];
    },
    getById: function(id) {
      var choices = {
        Sequence: User.getGrower(),
        Maingroup: User.getSelectedMaingroup(), // todo
        ShowAll: 0,
        OfferNo: id
      };       
      var apiUrl =  apiBaseUrl +'/Offerte/listedit';  
      return $http.post(apiUrl, choices).then(function(response) {
        console.log(response.data);
        return response.data;
      });    
    },
    getBuyersByNo: function(id) {
      var choices = {
        Sequence: User.getGrower(),
        Maingroup: User.getSelectedMaingroup(), // todo
        ShowAll: 0,
        OfferNo: id
      };       
      var apiUrl =  apiBaseUrl +'/Offerte/listoffertebuyers';  
      return $http.post(apiUrl, choices).then(function(response) {
        console.log(response.data);
        return response.data;
      }); 
    },
    getList: function(id) {
      var choices = {
        Sequence: id,
        Maingroup: User.getSelectedMaingroup(),
        ShowAll: 0
      };      
      var apiUrl =  apiBaseUrl +'/Offerte/list';  
      console.log(apiUrl);
      return $http.post(apiUrl, choices).then(function(response) {
        currentOfferteList = response.data;
        return response.data;
      });          
    },
    getOfferNo: function() {
      var choices = {
        Sequence: User.getGrower(),
        Maingroup: User.getSelectedMaingroup()
      };      
      var apiUrl =  apiBaseUrl +'/Offerte/offerno';  
      return $http.post(apiUrl, choices).then(function(response) {
        console.log(response.data);
        return response.data;
      });          
    },  
    getNextWorkingDay: function() {
      var apiUrl = apiBaseUrl + '/Offerte/nextworkingday';
      return $http.get(apiUrl).then(function(response) {
        console.log(response.data);
        return response.data;
      });
    }, 
    getByNr: function(id) {
      if(currentOfferteList == null) return null;
      for(var i=0;i<currentOfferteList.length;i++) {
        if(currentOfferteList[i].OFHD_NO == id) return currentOfferteList[i];
      }
    },
    getBuyersList: function() {
      var choices = {
        Sequence: User.getGrower(),
        Maingroup: User.getSelectedMaingroup(), // todo
      };       
      var apiUrl =  apiBaseUrl +'/Offerte/listbuyers';  
      console.log(apiUrl);
      return $http.post(apiUrl, choices).then(function(response) {
        //currentOfferteList = response.data;
        $localstorage.setObject('buyerlist', response.data);
        return response.data;
      });
    },
    publish: function(buyers) {
      var current_offerte_data = $localstorage.getObject('current_offerte_data');
      if(current_offerte_data == undefined) return false;

      current_offerte_data.buyers = buyers;
      console.log('current_offerte_data:', current_offerte_data);


      console.log('$rootScope.tmpStorage.fromDate/toDate: ', $rootScope.tmpStorage.fromDate, $rootScope.tmpStorage.toDate);
      console.log('$rootScope.tmpStorage.fromDate/toDate: ', new Date($rootScope.tmpStorage.fromDate), new Date($rootScope.tmpStorage.toDate));

      var fromDate = new Date($rootScope.tmpStorage.fromDate);
      var toDate = new Date($rootScope.tmpStorage.toDate)

      var choices = {
        Sequence: User.getGrower(),
        Maingroup: User.getSelectedMaingroup(),
        Offerte: current_offerte_data,
        fromDate: fromDate.getTime(),
        toDate: toDate.getTime(),
        fromTime: $localStorage.fromTime,
        toTime: $localStorage.toTime
      };       
      var apiUrl =  apiBaseUrl +'/Offerte/publish';  
      return $http.post(apiUrl, choices).then(function(response) {
        $localstorage.setObject('current_offerte_data', undefined);
        return response.data;
      });
    },
    saveOfferToLocal: function(offer) {
      console.log('Offer saving to LocalStorage: ', offer);
      console.log('Lets store the selected offer to localstorage, so the grower can add/remove/modify articles untill satisfied before submitting it.');
      $localstorage.setObject('copyoffer', offer);      
      // Now add the current offer to our NewOffer-object in localstorage
      var newOffer = [];   
      for(var i = 0; i< offer.length; i++) {
        newOffer.push({ 
          GRAS_SEQUENCE: offer[i].GRAS_SEQUENCE,
          CACA_SEQUENCE: offer[i].OFDT_FK_CACA_SEQ,
          PIC_SEQUENCE: offer[i].PIC_SEQ,
          NUM_ITEMS: offer[i].OFDT_NUM_OF_ITEMS,
          PRICE: offer[i].OFDT_ITEM_PRICE,
          REMARK: offer[i].OFDT_REMARK,
          SPECIAL: offer[i].IsSpecial
        });        
      }
      $localstorage.setObject('newoffer', newOffer); 
      console.log('New template offer: ', newOffer);
    },
    addToOffer: function(article) {
      var newOffer = $localstorage.getObject('newoffer');
      newOffer.push({ 
          GRAS_SEQUENCE: article.seq,
          CACA_SEQUENCE: 0,
          PIC_SEQUENCE: article.pictureseq,
          NUM_ITEMS: article.numitems,
          PRICE: 0,
          REMARK: 'testremark',
          SPECIAL: 1
        });
      $localstorage.setObject('newoffer', newOffer); 
      console.log('Adding article to LocalStorage-offer: ', article);
    },
    deleteOfferte: function(nr) {
      var choices = {
        Sequence: User.getGrower(),
        OfferNo: nr
      };       
      var apiUrl =  apiBaseUrl +'/Offerte/delete';  
      return $http.post(apiUrl, choices).then(function(response) {
        console.log('Result from deleteOfferte: ', response.data);
        return response.data;
      });      
    },
    modifyRow: function(row) {
      var apiUrl = apiBaseUrl + '/Offerte/modify';
      return $http.post(apiUrl, row).then(function(response) {
        console.log('Result from modifyRow: ', response.data);
        return response.data;
      });
    }
  };
});