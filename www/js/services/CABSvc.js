angular.module('LogiCAB.services')
.factory('CAB', function($http, User) {
  var apiBaseUrl = User.getEnviroment().url;
  return {
    
    getNew: function() {
      var apiUrl =  apiBaseUrl +'/CAB/new';  
      return $http.get(apiUrl).then(function(response) {
        console.log(response.data);
        return response.data;
      });          
    }
  };
});