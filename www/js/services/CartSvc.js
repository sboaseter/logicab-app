angular.module('LogiCAB.services')
.factory('Cart', function($state, $http, $localstorage, Offers, User, Order) {

  var cart = {};
  cart.mainGroup = -1;
  cart.selectedDate = -1;
  cart.items = [];
  cart.confirmed = false;

  cart.resetCart = function() {
    cart.items = [];
    cart.confirmed = false;
    cart.selectedDate = -1;
  }

  cart.isConfirmed = function() {
    return cart.confirmed;
  }
  cart.isEmpty = function() {
    if(cart.items.length == 0) {
        return true;
    } 
    return false;
  }

  cart.confirmOrder = function() {
    console.log(cart.items.length, " items to confirm!");
//      this.setEnviroment(enviroment);
      var apiUrl =  User.getEnviroment().url +'/Order/confirm';    
      
      var user = User.getUser(); // get full userobject

      var order = {
        Buyer: user.buyer,
        Grower: cart.items[0].offer.SP,
        Person: user.person,
        Offertype: cart.items[0].offer.OT,
        Maingroup: cart.items[0].offer.MG,
        SelectedDate: cart.selectedDate,
        Items: cart.itemsToJson()
      };
    return $http.post(apiUrl, order).then(function(response) {
      console.log(response);
      cart.confirmed = true;


    });

  };

cart.itemsToJson = function() {
  var ret = [];
  for(var i =0;i<cart.items.length;i++) {
    ret.push({
      Sequence: cart.items[i].offer.S,
      Numitems: cart.items[i].num,
      Custcode: cart.items[i].custcode,
      Sticker: cart.items[i].sticker,
      Status: 1
    });
  }
  return ret;

};
  cart.getSelectedDate = function() {
    return cart.selectedDate;

  };

  cart.addToCart = function(offer, num, custcode, sticker) {
      // Check on maingroup and date, must be top-level properties of  cart-model
//      var cart = $localstorage.getObject('cart');

      if(cart.items.length > 0) {
        if(cart.selectedDate != User.getSelectedDate()) return false;
        // Validate if requested offerdetail matches current Cart-content (offertype, maingroup etc)
        cart.items.push({offer: offer, num: num, custcode: custcode, sticker: sticker}); // more elaborate object
        return true;        
//        $localstorage.setObject('cart', cart);
      } else {
        cart.selectedDate = User.getSelectedDate();
        //var cartArray = [];
//        cart = []; // new cart(maingroup, date)
        //cartArray.push({offer: offer, num: num});
        cart.items.push({offer: offer, num: num, custcode: custcode, sticker: sticker});
        return true;
//        $localstorage.setObject('cart', cartArray);
      }
      console.log('Added: ', offer.S, ' x ', num);
    };
//  var cartModel
    cart.removeFromCart = function(order) {
//      var cart = $localstorage.getObject('cart');
      if(cart.items != null) {
        for(var i=0;i<cart.items.length;i++) {
          if(cart.items[i].offer.S == order.offer.S) {
            console.log('Found it: ', cart.items[i].offer.S);
            cart.items.splice(i, 1);
  //          $localstorage.setObject('cart', cart);

          }
        }
      }       
    };
    cart.addOneUnit = function(order) {
//      var cart = $localstorage.getObject('cart');
      if(cart != null) {
        for(var i=0;i<cart.items.length;i++) {
          if(cart.items[i].offer.S == order.offer.S) {          
            cart.items[i].num = parseInt(cart.items[i].num) + 1;
            console.log('Adding 1 unit of: ', cart.items[i].offer.PCD);

            //cart.splice(i, 1);
  //          $localstorage.setObject('cart', cart);

          }
        }
      }      
    };
    cart.getCart = function() {
//      var cart = $localstorage.getObject('cart');
      if(cart.items.length > 0) {
        return cart.items;
      }
      return null;
    };
    cart.getTotalValue = function() {
  //    var cart = $localstorage.getObject('cart');
      var totalValue = 0;
      if(cart.items != null) {
        console.log('Items in cart:', cart.items);
        for(var i=0;i<cart.items.length;i++) {
          totalValue = totalValue + cart.items[i].offer.Price * cart.items[i].offer.NOU; // todo: check?
        }
        return totalValue;
      }      
      return 0;
    };
    return cart;
});