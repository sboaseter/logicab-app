angular.module('LogiCAB.services')
.factory('User', function($state, $http, $localstorage, apiUrls, tourGuide) {
  var loggedIn = false;  
  return {
    setTourVisited: function(state) {
      var tour = $localstorage.getObject('tour');
      if(tour != null) {
        console.log('Setting tour to shown: ', state);
        for(var i=0;i<tour.length;i++) {
          if(tour[i].state == state) {
            tour[i].show = 0;
            console.log('Current tour: ', tour);
            this.setTour(tour);
            return true;
            
          }
        }
        return false;
      }
    },
    setTour: function(obj) {
      $localstorage.setObject('tour', obj);
    },
    getTour: function(state) {
      // Check showYN for requested state
      var tour = $localstorage.getObject('tour');
      if(tour != null) {
        for(var i=0;i<tour.length;i++) {
          if(tour[i].state == state) {
            console.log('Tour found: Show: ', tour[i].show);
            return tour[i].show == 1 ? tour[i].url : ''; // return tourUrl if show is set to 1
          }
        }
        return '';
      } else {        
        $localstorage.setObject('tour', tourGuide);
        return this.getTour(state);
      }
    },
    resetTour: function() {
      $localstorage.setObject('tour', tourGuide);
    },
    disableTour: function() {
      var tour = $localstorage.getObject('tour');
      if(tour != null) {
        for(var i=0;i<tour.length;i++) {
          tour[i].show = 0; // return tourUrl if show is set to 1
        }
        this.setTour(tour);
      }      
    },
    setEnviroment: function(enviroment) { // Candiate for Settings-page (Test)
      $localstorage.setObject('env', enviroment);
    },
    getEnviroment: function() {
      var env = $localstorage.getObject('env');
      if(env != null) {
        return env;
      } else {
        return apiUrls[0];
      }
    },
    setSettings: function(settings) {
      $localstorage.setObject('settings', settings);
    },
    getSettings: function() {
      var settings = $localstorage.getObject('settings');
      if(settings != null) {
        return settings;
      } else {
        this.setSettings({    
          "historyDays": 14,
          "notifications": false,
          "tour": true
        });
        return this.getSettings();
      }      
    },
    getChoices: function() {
      return {
        Buyer: this.getBuyer(),
        Maingroup: this.getSelectedMaingroup(),
        SelectedDate: this.getSelectedDate()
      };
    },
    setSelectedMaingroup: function(mg) {
      var user = $localstorage.getObject('user');
      if(user != null) {
        user.selectedMaingroup = mg;
        $localstorage.setObject('user', user);
      }
    },
    getSelectedMaingroup: function() {
      var user = $localstorage.getObject('user');
      if(user !== null) {        
        return user.selectedMaingroup;
      } else {
        return "";
      }
    },    
    setSelectedDate: function(date) {
      var user = $localstorage.getObject('user');
      if(user != null) {
        user.selectedDate = date;
        $localstorage.setObject('user', user);
      }
    },
    getSelectedDate: function() {
      var user = $localstorage.getObject('user');
      if(user !== null) {        
        return user.selectedDate;
      } else {
        return "";
      }
    },      
    getName: function() {
      var user = $localstorage.getObject('user');
      if(user !== null) {        
        return user.personName;
      } else {
        return "";
      }
    },
    getUser: function() {
      var user = $localstorage.getObject('user');
      if(user !== null) {
        return user;
      } else {
        $state.go('login');
      }
    },
    getBuyer: function() {
      var user = $localstorage.getObject('user');
      if(user !== null) {
        return user.buyer;
      } else {
        $state.go('login');
      }
    },
    getGrower: function() {
      var user = $localstorage.getObject('user');
      if(user !== null) {
        return user.grower;
      } else {
        $state.go('login');
      }      
    },
    isGrower: function() {
      var user = $localstorage.getObject('user');
      if(user !== null) {
        if(user.buyer != 0) return false;
        if(user.grower != 0) return true;
      } else {
        $state.go('login');
      }      
    },
    isBuyer: function() {
      var user = $localstorage.getObject('user');
      if(user !== null) {
        if(user.buyer != 0) return true;
        if(user.grower != 0) return false;
      } else {
        $state.go('login');
      }      
    },    
    validate: function(username, password, rememberme, enviroment) {
      var ret = false;
      if(enviroment === undefined) {
        this.setEnviroment(apiUrls[0]); // Default Production
      } else {
        this.setEnviroment(enviroment);
      }
      var apiUrl =  this.getEnviroment().url +'/User/validate';
      console.log('apiUrl: ', apiUrl);
      return $http.post(apiUrl, {Username:username, Password: password}).then(function(response) {
        console.log(response.data.Message);
        var resultCode = response.data.MessageID;
        console.log(resultCode);
        if(resultCode == 0) {
          console.log('success login: ', response.data);
          console.log('rememberme?' + rememberme);
          loggedIn = true;
          var li = response.data;
          $localstorage.setObject('user', {
            name: li.LOGI_USER_NAME,
            logiseq: li.LOGI_SEQUENCE,
            buyer: li.BUYR_SEQUENCE,
            grower: li.GROW_SEQUENCE,
            person: li.LOGI_FK_PERSON,
            orga: li.ORGA_SEQUENCE,
            orga_pic: li.ORGA_FK_PICTURE,
            personName: li.PERS_NAME,
            rememberme: rememberme == undefined ? false : true,
            selectedMaingroup: li.MAIN_GROUP,
            selectedDate: null
          });           
          return true;
        } else if(resultCode == 1) {          
          return false; //And something else? like message
        } else if(resultCode == 2) {
          return false; // And something else? like message
        } else {
          return false;
        }
      })
      .catch(function(response) {
        console.error('Connection error', response.status, response.data);
        return false;
      });            
    },
    getMaingroups: function() {
      var user = $localstorage.getObject('user');
      var apiUrl =  this.getEnviroment().url +'/User/maingroups/' + user.orga;
      
      return $http.get(apiUrl).then(function(response) {
        return response.data;
      });
    },
    rememberMe: function() {
      var user = $localstorage.getObject('user');
      if(user !== null) {
        if(user.rememberme) {
          return true;
        } else {
          return false;
        }
      }
      return false;
    },
    logout: function() {
      console.log('Forgetting user!');
      $localstorage.setObject('user', null);
      return true;
    },
    registerUser: function(newuser) {    
      var apiUrl =  this.getEnviroment().url +'/User/newuser';  
      return $http.post(apiUrl, newuser).then(function(response) {
        console.log(response.data);
//        var data = response.data;
      });        
    },
    sendFeedback: function(frmData) {
      var apiUrl = this.getEnviroment().url + '/User/feedback';
      var u = this.getUser();
      var ls = u.logiseq;
      console.log('ToSend: ', { username: u.name,  text: frmData });
      return $http.post(
        apiUrl, 
        {
          username: u.name, 
          text: frmData
        }).then(function(response) {
        console.log(response.data);
      });
    }
  }
});