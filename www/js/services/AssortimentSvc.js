angular.module('LogiCAB.services')
.factory('Assortiment', function($http, User) {
  return {
    getList: function() {
      var choices = {
        Sequence: User.getGrower(),
        Maingroup: User.getSelectedMaingroup(), // todo
        ShowAll: 0,
        OfferNo: ''
      };      
      var apiUrl = User.getEnviroment().url + '/Assortiment/list';
      return $http.post(apiUrl, choices).then(function(response) {
        //console.log(response.data);
        return response.data;
      });
    }

  };
});