angular.module('LogiCAB.services')
.factory('Camera', function($q, User, $http, $cordovaFile, FileService) {

  function makeid() {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
 
    for (var i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  };

  return {
    getPicture: function(options) {
      var q = $q.defer();

       var options = {
         quality: 50,
         destinationType : Camera.DestinationType.FILE_URI,
         sourceType : Camera.PictureSourceType.CAMERA, // Camera.PictureSourceType.PHOTOLIBRARY
         allowEdit : false,
         encodingType: Camera.EncodingType.JPEG,
         popoverOptions: CameraPopoverOptions,
       };      

      navigator.camera.getPicture(function(result) {
        q.resolve(result);
      }, function(e) {
        q.reject(err);
        console.log('Error? : ', e);
      }, options);        
        
      return q.promise;
    },
    sendPicture: function(imguri) {
      var apiUrl =  User.getEnviroment().url +'/Echo/upload';    
      //var lC = LZString.compressToUTF16(imguri);
      console.log('About to transmit image: length: ', imguri.length, ' to: ', apiUrl);

      return $http.post(apiUrl, {seq:1, data: imguri}).then(function(response) {
        return response.data;
      });      
    }
  }
});