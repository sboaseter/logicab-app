angular.module('LogiCAB.services', [])
.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      var v = $window.localStorage[key];
      if(v != null && v != "undefined" && v != undefined) {
        return JSON.parse($window.localStorage[key] || null);
      } else {
        return null;
      }
    }
  }
}]);
